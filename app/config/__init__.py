from os import getenv

class BaseConfig:
    SQLALCHEMY_DATABASE_URI = getenv('DATABASE_URL')


class DevelopmentConfig(BaseConfig):
    pass


class ProductionConfig(BaseConfig):
    pass

environment = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}