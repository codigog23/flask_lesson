from flask_restx import fields
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from app.models.users_model import UserModel


class UserRequestSchema:
    def __init__(self, namespace):
        self.ns = namespace

    def create(self):
        return self.ns.model('User Create', {
            'name': fields.String(required=True, max_length=80),
            'last_name': fields.String(required=True, max_length=100),
            'age': fields.Integer(required=True)
        })
    
    def update(self):
        return self.ns.model('User Update', {
            'name': fields.String(required=True, max_length=80),
            'last_name': fields.String(required=True, max_length=100),
            'age': fields.Integer(required=True)
        })

class UserResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = UserModel
