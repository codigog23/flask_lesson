from app.models import BaseModel
from sqlalchemy import Column, Integer, VARCHAR

class UserModel(BaseModel): # user_models
    __tablename__ = 'users'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(VARCHAR(80))
    last_name = Column(VARCHAR(100))
    age = Column(Integer)
