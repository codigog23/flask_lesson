from app import db
from app.models.users_model import UserModel
from app.schemas.users_schema import UserResponseSchema
from http import HTTPStatus

class UserController:
    def __init__(self):
        self.db = db
        self.model = UserModel
        self.schema = UserResponseSchema

    def fetch_all(self):
        # SELECT * FROM users;
        records = self.model.all()
        response = self.schema(many=True)
        return response.dump(records)
    
    def save(self, body):
        try:
            # INSERT INTO users () VALUES ()
            record = self.model.create(**body)
            return {
                'message': f'El usuario {record.name} se creo con exito'
            }, HTTPStatus.CREATED
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def find_by_id(self, id):
        try:
            # SELECT * FROM users WHERE id = 1
                                # columna=valor
            record = self.model.where(id=id).first()

            if not record:
                return {
                    'message': f'No se encontro un usuario con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            response = self.schema(many=False)
            return response.dump(record)
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def update(self, id, body):
        try:
            record = self.model.where(id=id).first()

            if not record:
                return {
                    'message': f'No se encontro un usuario con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            record.update(**body)
            return {
                'message': f'El usuario con el ID: {id} ha sido actualizado'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def remove(self, id):
        try:
            record = self.model.where(id=id).first()

            if not record:
                return {
                    'message': f'No se encontro un usuario con el ID: {id}'
                }, HTTPStatus.NOT_FOUND

            record.delete()
            return {
                'message': f'El usuario con el ID: {id} ha sido eliminado'
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
