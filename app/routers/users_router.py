from app import api
from flask import request
from flask_restx import Resource
from app.controllers.users_controller import UserController
from app.schemas.users_schema import UserRequestSchema

# http://api.dominio.com/users

user_ns = api.namespace(
    name='Usuarios',
    description='Endpoints del modulo Usuario',
    path='/users'
)

schema_request = UserRequestSchema(user_ns)

# CRUD
@user_ns.route('')
class UserRouter(Resource):
    def get(self):
        ''' Listar los usuarios '''
        controller = UserController()
        return controller.fetch_all()

    @user_ns.expect(schema_request.create(), validate=True)
    def post(self):
        ''' Crear un usuario '''
        controller = UserController()
        return controller.save(request.json)

@user_ns.route('/<int:id>')
class UserByIdRouter(Resource):
    def get(self, id):
        ''' Obtener usuario por ID '''
        controller = UserController()
        return controller.find_by_id(id)

    @user_ns.expect(schema_request.update(), validate=True)
    def put(self, id):
        ''' Actualizar usuario por ID '''
        controller = UserController()
        return controller.update(id, request.json)

    def delete(self, id):
        ''' Eliminar usuario por ID '''
        controller = UserController()
        return controller.remove(id)
