![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

# Arquitectura Cliente <> Servidor

![img1](https://miro.medium.com/v2/resize:fit:1400/0*wDP2n9WQKGAyM53B.png)

## Verbos HTTP

- GET -> Traer data
- POST -> Creación data
- PUT -> Actualizar data
- PATCH -> Actualizar data
- DELETE -> Eliminar data

```json
// PUT -> Se debe enviar todo el objeto junto con los campos actualizados
{
    "name": "Enzo",
    "last_name": "Gutierrez",
    "username": "enzo.gutierrez"
}

// PATCH -> Se puede enviar los campos parcialmente, de lo que se ha actualizado
{
    "username": "enzo.gutierrez"
}
```

## Codigos de Estado

| Codigo Estado | Descripción                |
| ------------- | -------------------------- |
| **1xx**       | **Informativos**           |
| 102           | Procesando                 |
| **2xx**       | **Exito**                  |
| 200           | OK                         |
| 201           | Creado                     |
| 204           | Sin contenido              |
| **3xx**       | Redirecciónes              |
| **4xx**       | **Error de Cliente**       |
| 400           | Solicitud Incorrecta       |
| 401           | No autorizado              |
| 403           | Prohibido                  |
| 404           | No encontrado              |
| **5xx**       | **Error de Servidor**      |
| 500           | Error interno del servidor |

# ApiRest

1. Uso de URIs
2. Metodos HTTP (Verbos)
3. Formato de intercambio estandar

## Endpoint

- URL Base: https://backend.miservicio.com

Gestionar Usuarios

1. Path x Modulo: https://backend.miservicio.com/users

- CREATE | (POST) | IGUAL
- READ | (GET)
  - Un solo objeto
    - **Path Params**: https://backend.miservicio.com/users/{id}
  - Mas de un objeto
    - **QueryString** (filtros): : https://backend.miservicio.com/users?username=pepito&status=1
- UPDATE | (PUT / PATCH) | https://backend.miservicio.com/users/{id}
- DELETE | (DELETE) | https://backend.miservicio.com/users/{id}

## Framework

## Microframework

# Patron de diseño

## MVC (Modelo - Vista - Controlador)

1. Modelo (Model)
2. Vista (View)
3. Controlador (Controller)
