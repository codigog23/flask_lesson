# Flask

1. Instalación

```sh
pip install Flask
-----------------
pipenv install Flask
```

2. Variables de entorno

```sh
pip install python-dotenv
-------------------------
pipenv install python-dotenv
```

Archivo **.env**

```py
FLASK_APP='main.py'
FLASK_RUN_HOST=127.0.0.1
FLASK_RUN_PORT=5000
FLASK_DEBUG=True
FLASK_ENV='development'

DATABASE_URL='postgresql://usuario:contraseña@ip_servidor:puerto_servidor/nombre_bd'
```

### Conexión URI a PGSQL

```py
postgresql://usuario:password@ip_servidor:puerto/nombre_bd
```

## Migraciones

- Iniciar el paquete alembic (se ejecuta una sola, siempre y cuando, no exista la carpeta **migrations**).

```sh
flask db init
```

- Cada creación o actualización en nuestros modelos

```sh
flask db migrate -m "comentario"
```

- Sincronizar las migraciones

```sh
flask db upgrade
```

# Documentación

- SQLAlchemy

  - [Metodos usados para el modelo](https://docs.sqlalchemy.org/en/14/orm/query.html#sqlalchemy.orm.Query.all)
  - [Tipo de datos](https://docs.sqlalchemy.org/en/14/core/types.html)
  - [Nuevos metodos para el modelo](https://github.com/absent1706/sqlalchemy-mixins/blob/master/README.md)

- FlaskRestX

  - [Tipo de datos](https://flask-restx.readthedocs.io/en/latest/_modules/flask_restx/fields.html)
  - [Swagger](https://flask-restx.readthedocs.io/en/latest/swagger.html)
